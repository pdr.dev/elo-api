### Desafio back end - Elo

##### Orientações para compilação e testes da aplicação:

###### Premissas: 
- Ter acesso à algum banco de dados Oracle (conforme solicitado via desafio).
- Executar os scripts disponibilizados na pasta "scripts" localizada na rais do projeto.
- <a href="https://projectlombok.org/setup/eclipse"> Instalação da biblioteca Lombok. </a>
- Ter Configurado maven para versionamento de dependências.

###### Passos:
- Cadastro das variáveis de ambiente para realização de conexão ao banco de dados.
<ol>
	<li> ORACLE_URL deve seguir o seguinte padrão: jdbc:oracle:thin:@<strong>servidor</strong>:<strong>1521</strong> (exemplo: jdbc:oracle:thin:@localhost:1521). </li>
	<li> ORACLE_USERNAME se refere ao usuário a ser utilizado. </li>
	<li> ORACLE_PASSWORD se refere a senha a ser utilizada. </li>
</ol>

- Carga inicial: conforme passado para dar inicio ao desafio, desenvolvi um pequeno job carregando as tabelas de cliente e compra.

- Endpoints: 
<ol>
	<li>http://localhost:5000/cliente/status/<strong>status do cliente (A ou I)</strong> - Retorna os clientes de acordo com status passado.</li>
	<li>http://localhost:5000/cliente/mais-compras - Retorna os 10 clientes que mais realizaram compras.</li>
	<li>http://localhost:5000/cliente/compras-mais-caras - Retorna os 10 clientes que realizaram as compras mais caras.</li>
	<li>http://localhost:5000/compra - Método GET - Listagem de todas as compras.</li>
	<li>http://localhost:5000/compra/<strong>id da compra</strong> - Método PUT - Alteração de uma determinada compra</li>
	<li>http://localhost:5000/compra - Método POST - Inserção de uma nova compra.</li>
	<li>http://localhost:5000/compra/<strong>id da compra</strong> - Método DELETE - Exclusão de determinada compra.</li>
	<li>http://localhost:5000/compra/cliente/<strong>id da compra</strong> - Método GET - Realiza a listagem das compras de determiando cliente. </li>
	<li>http://localhost:5000/compra/cliente/<strong>id da compra</strong>/<strong>data da compra (formato: yyyy-MM-dd)</strong> - Método GET - Realiza a listagem das compras de determiando cliente.</li>
</ol>

###### Payload de exemplo:
```
{
    "descricao": "Chuveirada",
    "valor": 73.76,
    "tipo": "C",
    "dataCompra": "2021-12-02T03:00:00.000+00:00",
    "cliente": {
      "id": 3732,
      "nome": "Kokour",
      "cnpj": "04.830.361/0001-16",
      "dataInclusao": "2021-02-04T03:00:00.000+00:00",
      "status": "A"
    }
}
```




