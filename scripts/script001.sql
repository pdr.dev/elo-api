CREATE TABLE cliente(
	id_cliente NUMBER,
	nome_cliente VARCHAR2(100) NOT NULL,
	cnpj VARCHAR(18),
	data_inclusao DATE,
	status VARCHAR2(1),
	PRIMARY KEY(id_cliente)
);

CREATE SEQUENCE sequence_cliente
START WITH 1
INCREMENT BY 1
CACHE 10;

CREATE OR REPLACE TRIGGER trigger_sequence_cliente
BEFORE INSERT
ON cliente
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
  if(:new.id_cliente is null) then
  SELECT sequence_cliente.nextval
  INTO :new.id_cliente
  FROM dual;
  end if;
END;

ALTER TRIGGER trigger_sequence_cliente ENABLE;