CREATE TABLE compra(
	id_compra NUMBER,
	desc_compra VARCHAR(1024) NOT NULL,
	valor_compra NUMBER(19,2) NOT NULL,
	tipo_compra VARCHAR2(1) NOT NULL,
	data_compra DATE,
	id_cliente NUMBER,
	PRIMARY KEY(id_compra),
	FOREIGN KEY(id_cliente) REFERENCES cliente(id_cliente)
);

CREATE SEQUENCE sequence_compra
START WITH 1
INCREMENT BY 1
CACHE 10;

CREATE OR REPLACE TRIGGER trigger_sequence_compra
BEFORE INSERT
ON compra
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
  if(:new.id_compra is null) then
  SELECT sequence_compra.nextval
  INTO :new.id_compra
  FROM dual;
  end if;
END;

ALTER TRIGGER trigger_sequence_compra ENABLE;