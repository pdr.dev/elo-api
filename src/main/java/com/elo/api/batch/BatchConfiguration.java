package com.elo.api.batch;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.elo.api.model.Cliente;
import com.elo.api.model.Compra;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	private final JobBuilderFactory jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;

	private final DataSource dataSource;

	public BatchConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
			DataSource dataSource) {
		this.jobBuilderFactory = jobBuilderFactory;
		this.stepBuilderFactory = stepBuilderFactory;
		this.dataSource = dataSource;
	}

	@Bean
	public FlatFileItemReader<Cliente> readerDataCliente() {
		FlatFileItemReader<Cliente> reader = new FlatFileItemReader<>();
		reader.setResource(new ClassPathResource("data-cliente.csv"));
		reader.setLineMapper(new DefaultLineMapper<Cliente>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "id", "nome", "cnpj", "dataInclusao", "status" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Cliente>() {
					{
						setTargetType(Cliente.class);
					}
				});
			}
		});
		return reader;
	}

	@Bean
	public FlatFileItemReader<Compra> readerDataCompra() {
		FlatFileItemReader<Compra> reader = new FlatFileItemReader<>();
		reader.setResource(new ClassPathResource("data-compra.csv"));
		reader.setLineMapper(new DefaultLineMapper<Compra>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "cliente.id", "id", "descricao", "valor", "tipo", "dataCompra" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Compra>() {
					{
						setTargetType(Compra.class);
					}
				});
			}
		});
		return reader;
	}

	@Bean
	public JdbcBatchItemWriter<Cliente> writerDataCliente() {
		JdbcBatchItemWriter<Cliente> writer = new JdbcBatchItemWriter<>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
		writer.setSql(
				"INSERT INTO cliente (id_cliente, nome_cliente, cnpj, data_inclusao, status) VALUES (:id, :nome, :cnpj, :dataInclusao, :status)");
		writer.setDataSource(this.dataSource);
		return writer;
	}

	@Bean
	public JdbcBatchItemWriter<Compra> writerDataCompra() {
		JdbcBatchItemWriter<Compra> writer = new JdbcBatchItemWriter<>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
		writer.setSql(
				"INSERT INTO compra (id_cliente, id_compra, desc_compra, valor_compra, tipo_compra, data_compra) VALUES (:cliente.id, :id, :descricao, :valor, :tipo, :dataCompra)");
		writer.setDataSource(this.dataSource);
		return writer;
	}

	@Bean
	public Job importJob() {
		return jobBuilderFactory.get("importClienteJob").flow(stepCliente1()).next(stepCompra1()).end().build();
	}

	@Bean
	public Step stepCliente1() {
		return stepBuilderFactory.get("stepCliente1").<Cliente, Cliente>chunk(10).reader(readerDataCliente())
				.writer(writerDataCliente()).build();
	}

	@Bean
	public Step stepCompra1() {
		return stepBuilderFactory.get("stepCompra1").<Compra, Compra>chunk(10).reader(readerDataCompra())
				.writer(writerDataCompra()).build();
	}
}