package com.elo.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elo.api.exception.ClienteInexistenteException;
import com.elo.api.model.Cliente;
import com.elo.api.repository.ClienteRepository;
import com.elo.api.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public Optional<Cliente> create(Cliente cliente) {
		return Optional.ofNullable(this.clienteRepository.save(cliente));
	}

	@Override
	public Optional<List<Cliente>> read() {
		List<Cliente> clientes = this.clienteRepository.findAll();
		if (clientes.isEmpty())
			return Optional.empty();

		return Optional.ofNullable(clientes);
	}

	@Override
	public Optional<Cliente> findById(Long id) {
		Optional<Cliente> cliente = this.clienteRepository.findById(id);

		if (cliente.isPresent())
			return cliente;
		else
			throw new ClienteInexistenteException("Cliente não encontrado na base de dados.");
	}

	@Override
	public ResponseEntity<?> findByStatus(String status) {
		return ResponseEntity.ok(this.clienteRepository.findByStatus(status));
	}

	@Override
	public ResponseEntity<?> readFromMaisCompras() {
		return ResponseEntity.ok(this.clienteRepository.findByClienteMaisCompras());
	}

	@Override
	public ResponseEntity<?> readFromComprasMaisCaras() {
		return ResponseEntity.ok(this.clienteRepository.findByClienteComprasMaisCaras());
	}
}