package com.elo.api.service.impl;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elo.api.exception.ClienteInexistenteException;
import com.elo.api.exception.CompraException;
import com.elo.api.exception.CompraInexistenteException;
import com.elo.api.model.Cliente;
import com.elo.api.model.Compra;
import com.elo.api.repository.CompraRepository;
import com.elo.api.service.ClienteService;
import com.elo.api.service.CompraService;

@Service
public class CompraServiceImpl implements CompraService {

	@Autowired
	private CompraRepository compraRepository;
	@Autowired
	private ClienteService clienteService;

	@Override
	public ResponseEntity<?> create(Compra compra) {
		validarRequisicoesCompra(compra);
		return ResponseEntity.ok(Optional.ofNullable(this.compraRepository.save(compra)));
	}

	@Override
	public ResponseEntity<?> read() {
		return ResponseEntity.ok(Optional.ofNullable(this.compraRepository.findAll()));
	}

	@Override
	public ResponseEntity<?> update(Long id, Compra compra) {
		Optional<Compra> compraExistente = this.compraRepository.findById(id);

		if (compraExistente.isEmpty())
			throw new CompraInexistenteException(
					String.format("Compra com o id \"%s\" não foi encontrada em nossa base de dados.", id));

		return ResponseEntity.ok(Optional.ofNullable(this.compraRepository.save(compra)));
	}

	@Override
	public void delete(Long id) {
		this.compraRepository.deleteById(id);
	}

	@Override
	public ResponseEntity<?> findById(Long id) {
		Optional<Compra> compra = this.compraRepository.findById(id);

		if (compra.isEmpty())
			throw new CompraInexistenteException(
					String.format("Compra com o id \"%s\" não foi encontrada em nossa base de dados.", id));

		return ResponseEntity.ok(compra);
	}

	@Override
	public ResponseEntity<?> findByIdCliente(Long idCliente) {
		return ResponseEntity.ok(this.compraRepository.findByIdCliente(idCliente));
	}

	@Override
	public ResponseEntity<?> findByIdAndDataCompra(Long idCliente, Date dataCompra) {
		return ResponseEntity.ok(this.compraRepository.findByIdClienteAndDataCompra(idCliente, dataCompra));
	}

	private void validarRequisicoesCompra(Compra compra) {
		if (!("D".equals(compra.getTipo()) || "C".equals(compra.getTipo())))
			throw new CompraException("Tipo da compra deve ser informado C para crédito e D para débito.");
		else if (compra.getId() != null) {
			Optional<Compra> compraExistente = this.compraRepository.findById(compra.getId());
			if (compraExistente.isPresent())
				throw new CompraException("Já existe uma compra com esse id.");
		} else if (compra.getCliente() == null)
			throw new CompraException("É necessário informar um cliente para compra.");
		else {
			Optional<Cliente> clienteExistente = null;
			try {
				clienteExistente = this.clienteService.findById(compra.getCliente().getId());
			} catch (ClienteInexistenteException ex) {
				clienteExistente = this.clienteService.create(compra.getCliente());
				compra.setCliente(clienteExistente.get());
			}
		}
	}
}