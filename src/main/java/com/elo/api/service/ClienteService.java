package com.elo.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.elo.api.model.Cliente;

public interface ClienteService {

	public Optional<Cliente> create(Cliente cliente);

	public Optional<List<Cliente>> read();

	public Optional<Cliente> findById(Long id);

	public ResponseEntity<?> findByStatus(String status);

	public ResponseEntity<?> readFromMaisCompras();

	public ResponseEntity<?> readFromComprasMaisCaras();
}
