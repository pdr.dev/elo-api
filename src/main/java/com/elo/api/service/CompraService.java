package com.elo.api.service;

import java.util.Date;

import org.springframework.http.ResponseEntity;

import com.elo.api.model.Compra;

public interface CompraService {

	public ResponseEntity<?> create(Compra compra);

	public ResponseEntity<?> read();

	public ResponseEntity<?> update(Long id, Compra rede);

	public void delete(Long id);

	public ResponseEntity<?> findById(Long id);

	public ResponseEntity<?> findByIdCliente(Long idCliente);

	public ResponseEntity<?> findByIdAndDataCompra(Long idCliente, Date dataCompra);
}