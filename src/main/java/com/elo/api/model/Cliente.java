package com.elo.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_cliente")
	@SequenceGenerator(name = "sequence_cliente", sequenceName = "sequence_cliente", allocationSize = 1)
	@Column(name = "id_cliente")
	private Long id;

	@NotNull
	@Size(max = 50)
	@Column(name = "nome_cliente")
	private String nome;

	@NotNull
	@Size(max = 18)
	@Column(name = "cnpj")
	private String cnpj;

	@Column(name = "data_inclusao")
	private Date dataInclusao;

	@Column(name = "status")
	private String status = "A";
}