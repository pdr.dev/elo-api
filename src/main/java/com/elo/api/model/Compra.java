package com.elo.api.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Compra {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_compra")
	@SequenceGenerator(name = "sequence_compra", sequenceName = "sequence_compra", allocationSize = 1)
	@Column(name = "id_compra")
	private Long id;

	@Size(max = 1024)
	@Column(name = "desc_compra")
	private String descricao;

	@NotNull
	@Min(value = 0, message = "O valor da compra não pode ser negativo.")
	@Column(name = "valor_compra")
	private BigDecimal valor;

	@NotNull
	@Size(min = 1, max = 1, message = "Informar C para crédito ou D para débito.")
	@Column(name = "tipo_compra")
	private String tipo;

	@Column(name = "data_compra")
	private Date dataCompra;

	@NotNull
	@JoinColumn(name = "id_cliente", nullable = false)
	@ManyToOne(cascade = CascadeType.REMOVE)
	private Cliente cliente;
}