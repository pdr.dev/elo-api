package com.elo.api.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.elo.api.exception.ClienteException;
import com.elo.api.exception.ClienteInexistenteException;
import com.elo.api.exception.CompraException;
import com.elo.api.exception.CompraInexistenteException;
import com.elo.api.model.Error;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { Throwable.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		if (ex instanceof ClienteInexistenteException)
			return ResponseEntity.badRequest().body(new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
		else if (ex instanceof CompraInexistenteException)
			return ResponseEntity.badRequest().body(new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
		else if (ex instanceof CompraException)
			return ResponseEntity.badRequest().body(new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
		else if(ex instanceof ClienteException)
			return ResponseEntity.badRequest().body(new Error(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
		else
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
					new Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Ops! Ocorreu um erro interno no servidor."));
	}
}