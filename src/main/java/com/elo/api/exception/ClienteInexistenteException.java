package com.elo.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteInexistenteException extends ResponseStatusException {

	private static final long serialVersionUID = -8662803271429640360L;

	public ClienteInexistenteException(String message) {
		super(HttpStatus.BAD_REQUEST, message);
	}

	public ClienteInexistenteException(HttpStatus httpStatus, String message) {
		super(httpStatus, message);
	}
}