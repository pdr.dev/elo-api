package com.elo.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elo.api.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/status/{status}")
	public ResponseEntity<?> readFromStatus(@PathVariable String status) {
		return clienteService.findByStatus(status);
	}

	@GetMapping("/mais-compras")
	public ResponseEntity<?> readFromMaisCompras() {
		return clienteService.readFromMaisCompras();
	}

	@GetMapping("/compras-mais-caras")
	public ResponseEntity<?> readFromComprasMaisCaras() {
		return clienteService.readFromComprasMaisCaras();
	}
}