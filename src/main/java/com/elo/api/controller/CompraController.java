package com.elo.api.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.elo.api.model.Compra;
import com.elo.api.service.CompraService;

@RestController
@RequestMapping("/compra")
public class CompraController {

	@Autowired
	private CompraService compraService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@Valid @RequestBody Compra compra) {
		return this.compraService.create(compra);
	}

	@GetMapping
	public ResponseEntity<?> read() {
		return this.compraService.read();
	}

	@PutMapping("/{id_compra}")
	public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody Compra compra) {
		return this.compraService.update(id, compra);
	}

	@DeleteMapping("/{id_compra}")
	public void delete(@PathVariable Long id) {
		this.compraService.delete(id);
	}

	@GetMapping("/cliente/{id_cliente}")
	public ResponseEntity<?> readFromCliente(@PathVariable(value = "id_cliente") Long idCliente) {
		return this.compraService.findByIdCliente(idCliente);
	}

	@GetMapping("/cliente/{id_cliente}/{data_compra}")
	public ResponseEntity<?> readFromCliente(@PathVariable(value = "id_cliente") Long idCliente,
			@PathVariable(value = "data_compra") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dataCompra) {
		return this.compraService.findByIdAndDataCompra(idCliente, dataCompra);
	}

}