package com.elo.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.elo.api.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	Optional<Cliente> findById(Long id);

	Optional<List<Cliente>> findByStatus(String status);

	@Query(value = "select id_cliente, nome_cliente, cnpj, data_inclusao, status from (select count(compra.id_cliente), cliente.id_cliente, cliente.nome_cliente, cliente.cnpj,cliente.data_inclusao, cliente.status from cliente cliente "
			+ "inner join compra compra on compra.id_cliente = cliente.id_cliente "
			+ "group by cliente.id_cliente, cliente.nome_cliente, cliente.cnpj,cliente.data_inclusao, cliente.status "
			+ "order by 1 desc) where rownum <= 10", nativeQuery = true)
	Optional<List<Cliente>> findByClienteMaisCompras();

	@Query(value="select id_cliente, nome_cliente, cnpj, data_inclusao, status from ("
			+ "select compra.valor_compra, cliente.id_cliente, cliente.nome_cliente, cliente.cnpj,cliente.data_inclusao, cliente.status from cliente cliente "
			+ "inner join compra compra on compra.id_cliente = cliente.id_cliente "
			+ "order by compra.valor_compra desc) where rownum <= 10", nativeQuery =  true)
	Optional<List<Cliente>> findByClienteComprasMaisCaras();

}
