package com.elo.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.elo.api.model.Compra;

public interface CompraRepository extends JpaRepository<Compra, Long> {

	@Query(value = "select * from compra where id_cliente = :idCliente", nativeQuery = true)
	public List<Optional<Compra>> findByIdCliente(Long idCliente);

	@Query(value = "select * from compra where id_cliente = :idCliente and data_compra = :dataCompra", nativeQuery = true)
	public List<Optional<Compra>> findByIdClienteAndDataCompra(Long idCliente, Date dataCompra);

}