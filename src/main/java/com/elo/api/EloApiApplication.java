package com.elo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EloApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EloApiApplication.class, args);
	}
}